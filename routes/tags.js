const express = require('express')
const router = express.Router()
const Tag = require('../models/tag')

router.get('/:id', async (req, res) => {
    try {
        const tag = await Tag.findById(req.params.id)
        res.json(tag)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const tags = await Tag.find({})
        res.json(tags)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
        const tag = new Tag({
            name: req.body.name,
            slug: req.body.slug
        })
    try{
        const a1 = await tag.save()
        res.json(a1)
    }catch(err){
        res.send("Error" + err)
    }
})

module.exports = router