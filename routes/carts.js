const express = require('express')
const router = express.Router()
const Cart = require('../models/cart')

router.get('/:id', async (req, res) => {
    try {
        const cart = await Cart.findById(req.params.id)
        res.json(cart)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const carts = await Cart.find({})
        res.json(carts)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
    const cart = await new Cart({
        product: req.body.product,
        user: req.body.user,
        product_quantity: req.body.product_quantity,
        base_price: req.body.base_price,
        sell_price: req.body.sell_price,
        total_price: req.body.total_price
    })
    try {
        const a1 = await cart.save()
        res.json(a1)
    } catch (err) {
        res.send("Error" + err)
    }
})

module.exports = router