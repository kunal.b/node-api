const express = require('express')
const router = express.Router()
const Role = require('../models/role')

router.get('/:id', async (req, res) => {
    try {
        const role = await Role.findById(req.params.id)
        res.json(role)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const roles = await Role.find({})
        res.json(roles)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
    const role = new Role({
        name: req.body.name,
        slug: req.body.slug
    })

    try {
        const a1 = await role.save()
        res.json(a1)
    } catch (err) {
        res.send("Error" + err)
    }
})

module.exports = router