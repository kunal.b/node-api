const express = require('express')
const router = express.Router()
const Order = require('../models/order')

router.get('/:id', async (req, res) => {
    try {
        const order = await Order.findById(req.params.id)
        res.json(order)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const order = await Order.find({})
        res.json(order)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
    const order = await new Order({
        user_id: req.body.user_id,
        total_items: req.body.total_items,
        products: req.body.products,
        billing_address: req.body.billing_address,
        shipping_address: req.body.shipping_address,
        transactin_status: req.body.transactin_status,
        payment_mode: req.body.payment_mode,
        payment_status: req.body.payment_status
    })
    try {
        const a1 = await order.save()
        res.json(a1)
    } catch (err) {
        res.send("Error" + err)
    }
})

module.exports = router