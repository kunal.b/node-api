const express = require('express')
const router = express.Router()
const Category = require('../models/category')

router.get('/:id', async (req, res) => {
    try {
        const category = await Category.findById(req.params.id)
        res.json(category)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const categories = await Category.find({})
        res.json(categories)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
    const category = await new Category({
        name: req.body.name,
        slug: req.body.slug,
        image: req.body.image,
        description: req.body.description
    })

    try {
        const a1 = await category.save()
        res.json(a1)
    } catch (err) {
        res.send("Error" + err)
    }
})

module.exports = router