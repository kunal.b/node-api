const express = require('express')
const router = express.Router()
const Product = require('../models/product')

router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id)
        res.json(product)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.get('/', async (req, res) => {
    try {
        const products = await Product.find({})
        res.json(products)
    } catch (err) {
        res.send("Error" + err)
    }
})

router.post('/', async (req, res) => {
    const product = new Product({
        name: req.body.name,
        thumbnail: req.body.thumbnail,
        product_gallery: req.body.product_gallery,
        description: req.body.description,
        base_price: req.body.base_price,
        sell_price: req.body.sell_price,
        category_name: req.body.category_name,
        tags: req.body.tags
    })

    try {
        const a1 = await product.save()
        res.json(a1)
    } catch (err) {
        res.send("Error" + err)
    }
})

module.exports = router