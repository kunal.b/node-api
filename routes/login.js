const express = require('express')
const router = express.Router()

router.get('/', async (req, res)=>{
    res.send("Login Page")
})

router.get('/login', async (req, res)=>{
    res.send("Login Page")
})

router.get('/logout', async (req, res)=>{
    res.send("Logout Page")
})

module.exports = router