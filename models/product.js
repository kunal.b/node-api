const { Long, Decimal128 } = require('bson')
const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: String,
    thumbnail : String,
    product_gallery : String,
    description : String,
    base_price : Decimal128,
    sell_price : Decimal128,
    category_name : String,
    tags : Array
})

module.exports = mongoose.model('Product', productSchema)