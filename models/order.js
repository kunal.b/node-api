const mongoose = require('mongoose')

const orderSchema  = new mongoose.Schema({
    user_id : String,
    total_items : Number,
    products : Array,
    billing_address : String,
    shipping_address : String,
    transaction_status : String,
    payment_mode : String,
    payment_status : String
})

module.exports = mongoose.model('Order', orderSchema)