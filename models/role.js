const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
    name: String,
    slug: String
})

module.exports = mongoose.model('Role', roleSchema)