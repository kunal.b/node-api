const mongoose = require('mongoose')

const tagSchema = new mongoose.Schema({
    name: String,
    slug: String
})

module.exports = mongoose.model('Tag', tagSchema)