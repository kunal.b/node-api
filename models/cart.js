const { Decimal128 } = require('bson')
const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
    product : String,
    user : String,
    product_quantity : Number,
    base_price : Decimal128,
    sell_price : Decimal128,
    total_price : Decimal128
})

module.exports = mongoose.model('Cart', cartSchema)