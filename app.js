const express = require('express')
const mongoose = require('mongoose')

const url = 'mongodb://localhost/UserData'
const userRouter = require('./routes/users')
const roleRouter = require('./routes/roles')
const categoryRouter = require('./routes/categories')
const tagRouter = require('./routes/tags')
const productRouter = require('./routes/products')
const cartRouter = require('./routes/carts')
const orderRouter = require('./routes/orders')
const loginRouter = require('./routes/login')

mongoose.connect(url, {useNewUrlParser: true})
const db = mongoose.connection

db.on('open', ()=>{
    console.log("Connected.....");
})

const app = express()

app.use(express.json())

app.use('/', loginRouter)
app.use('/users', userRouter)
app.use('/roles', roleRouter)
app.use('/categories', categoryRouter)
app.use('/tags', tagRouter)
app.use('/products', productRouter)
app.use('/carts', cartRouter)
app.use('/orders', orderRouter)

app.listen(3000, ()=>{
    console.log("Server is running")
})